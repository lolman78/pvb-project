<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Deze combinatie van Email en wachtwoord zijn incorrect',
    'throttle' => 'Je hebt teveel pogingen gedaan om in te loggen. Probeer nog is in :seconds seconds.',

];
