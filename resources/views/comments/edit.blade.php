@extends('layouts.app')

@section('content')
    <div class="container">
        @if (session('danger'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                {{ session('danger') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif

        @if (session('success'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                {{ session('success') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif

        <div class="row justify-content-center">
            @can('isAdmin', Auth::user())
                <div class="col-md-3 p-0">
                    <div class="card">
                        <div class="card-header">
                            Admin menu
                        </div>
                        <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist"
                             aria-orientation="vertical">
                            <a class="nav-link" id="v-pills-team-tab" href="{{ route('admin.users') }}" role="tab"
                               aria-controls="v-pills-team" aria-selected="false">Users</a>
                            <a class="nav-link" id="v-pills-team-tab" href="{{ route('admin.blogs') }}"
                               role="tab" aria-controls="v-pills-team" aria-selected="false">Blogs</a>
                            <a class="nav-link" id="v-pills-club-tab" href="{{ route('admin.comments') }}" role="tab"
                               aria-controls="v-pills-club" aria-selected="true"><b>Comments</b></a>
                            <a class="nav-link" id="v-pills-user-tab" href="{{ route('admin.categories') }}" role="tab"
                               aria-controls="v-pills-user" aria-selected="false">Categorieen</a>
                            <a class="nav-link" id="v-pills-team-tab" href="{{ route('admin.roles') }}"
                               role="tab" aria-controls="v-pills-team" aria-selected="false">Rollen</a>
                        </div>
                    </div>
                </div>
            @endcan

            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Comment bewerken') }}</div>
                    <div class="card-body">

                        <div class="form-group row">
                            <label for="blog"
                                   class="col-md-4 col-form-label text-md-right">{{ __('Bijbehorende Blog') }}
                            </label>

                            <div class="col-md-6" style="margin-top: 8px">
                                {{ $comment->blog->title }}
                            </div>
                        </div>

                        <form method="POST" action="{{ route('comment.update', [$blog, $comment] )}}"
                              enctype="multipart/form-data">
                            @method('PUT')
                            @csrf

                            <div class="form-group row">
                                <label for="content"
                                       class="col-md-4 col-form-label text-md-right">{{ __('Inhoud') }}
                                </label>

                                <div class="col-md-6">
                                    <textarea id="content" class="form-control @error('content') is-invalid @enderror"
                                              name="content" autocomplete="content"
                                              rows="6">{{ old('content', $comment->content) }}
                                    </textarea>

                                    @error('content')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            {{--   hier moet later nog iets komen voor foto bij de blog doen   --}}

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Comment aanpassen') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
