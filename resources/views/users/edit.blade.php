@extends('layouts.app')

@section('content')
    <div class="container">
        @if (session('danger'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                {{ session('danger') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif

        @if (session('success'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                {{ session('success') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif

        <div class="row justify-content-center">
            <div class="col-md-3 p-0">
                <div class="card">
                    <div class="card-header">
                        Admin menu
                    </div>
                    <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                        <a class="nav-link" id="v-pills-team-tab" href="{{ route('admin.users') }}" role="tab"
                           aria-controls="v-pills-team" aria-selected="true"><b>Gebruikers</b></a>
                        <a class="nav-link" id="v-pills-team-tab" href="{{ route('admin.blogs') }}"
                           role="tab" aria-controls="v-pills-team" aria-selected="false">Blogs</a>
                        <a class="nav-link" id="v-pills-club-tab" href="{{ route('admin.comments') }}" role="tab"
                           aria-controls="v-pills-club" aria-selected="false">Comments</a>
                        <a class="nav-link" id="v-pills-user-tab" href="{{ route('admin.categories') }}" role="tab"
                           aria-controls="v-pills-user" aria-selected="false">Categorieen</a>
                        <a class="nav-link" id="v-pills-team-tab" href="{{ route('admin.roles') }}"
                           role="tab" aria-controls="v-pills-team" aria-selected="false">Rollen</a>
                    </div>
                </div>
            </div>

            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Gebuiker bewerken') }}</div>

                    <div class="card-body">
                        <div class="nav nav-tabs" id="nav-tab" role="tablist" style="margin-bottom: 20px;">
                            <a class="nav-item nav-link active" id="nav-profile-tab" data-toggle="tab"
                               href="#nav-profile" role="tab" aria-controls="nav-profile"
                               aria-selected="true">Profiel
                            </a>

                            <a class="nav-item nav-link" id="nav-password-tab" data-toggle="tab"
                               href="#nav-password" role="tab" aria-controls="nav-password"
                               aria-selected="false">Wachtwoord
                            </a>
                        </div>

                        <div class="tab-content" id="nav-tabContent">
                            <div class="tab-pane fade show active" id="nav-profile" role="tabpanel"
                                 aria-labelledby="nav-profile-tab">

                                <form method="POST" action="{{ route('user.update', $user) }}">
                                    @method('PUT')
                                    @csrf

                                    <div class="form-group row">
                                        <label for="name"
                                               class="col-md-4 col-form-label text-md-right">{{ __('Naam*') }}</label>

                                        <div class="col-md-6">
                                            <input id="name" type="text"
                                                   class="form-control @error('name') is-invalid @enderror" name="name"
                                                   value="{{ old('name', $user->name) }}" autocomplete="name" autofocus>

                                            @error('name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="email"
                                               class="col-md-4 col-form-label text-md-right">{{ __('Email*') }}</label>

                                        <div class="col-md-6">
                                            <input id="email" type="text"
                                                   class="form-control @error('email') is-invalid @enderror"
                                                   name="email"
                                                   value="{{ old('email', $user->email) }}" autocomplete="email"
                                                   autofocus disabled>

                                            @error('email')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="address"
                                               class="col-md-4 col-form-label text-md-right">{{ __('Adres') }}</label>

                                        <div class="col-md-6">
                                            <input id="address" type="text"
                                                   class="form-control @error('address') is-invalid @enderror"
                                                   name="address"
                                                   value="{{ old('address', $user->address) }}" autocomplete="address"
                                                   autofocus>

                                            @error('address')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="postalcode"
                                               class="col-md-4 col-form-label text-md-right">{{ __('Postcode') }}</label>

                                        <div class="col-md-6">
                                            <input id="postalcode" type="text"
                                                   class="form-control @error('postalcode') is-invalid @enderror"
                                                   name="postalcode"
                                                   value="{{ old('postalcode', $user->postalcode) }}"
                                                   autocomplete="postalcode"
                                                   autofocus>

                                            @error('postalcode')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="place"
                                               class="col-md-4 col-form-label text-md-right">{{ __('Plaats') }}</label>

                                        <div class="col-md-6">
                                            <input id="place" type="text"
                                                   class="form-control @error('place') is-invalid @enderror"
                                                   name="place"
                                                   value="{{ old('place', $user->place) }}" autocomplete="place"
                                                   autofocus>

                                            @error('place')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="phonenumber"
                                               class="col-md-4 col-form-label text-md-right">{{ __('Telefoon nummer') }}</label>

                                        <div class="col-md-6">
                                            <input id="phonenumber" type="text"
                                                   class="form-control @error('phonenumber') is-invalid @enderror"
                                                   name="phonenumber"
                                                   value="{{ old('phonenumber', $user->phonenumber) }}"
                                                   autocomplete="phonenumber" autofocus>

                                            @error('phonenumber')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="role_id" class="col-md-4 col-form-label text-md-right">Rol</label>

                                        <div class="col-md-6">
                                            <select id="role_id"
                                                    class="form-control @error('role_id') is-invalid @enderror"
                                                    name="role_id" value="{{ old('role_id', $user->role_id) }}" required
                                                    autocomplete="role_id"
                                                    autofocus>
                                                <option value="{{$user->role_id}}">{{ $user->role->name }}</option>
                                                @foreach($roles as $role)
{{--                                                als de role het zelfde is als die van de user maak het disabled--}}
                                                    @if($user->role_id == $role->id)
                                                        <option value="{{$role->id}}" disabled>{{$role->name}}</option>
                                                    @else
                                                        <option value="{{$role->id}}">{{$role->name}}</option>
                                                    @endif
                                                @endforeach
                                            </select>

                                            @error('role_id')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row mb-0">
                                        <div class="col-md-6 offset-md-4">
                                            <button type="submit" class="btn btn-primary">
                                                {{ __('Gebruiker bewerken') }}
                                            </button>

                                            <a class="text-secondary"
                                               href="{{ route('admin.users') }}">Terug
                                            </a>
                                        </div>
                                    </div>
                                </form>
                            </div>

                            <div class="tab-pane fade" id="nav-password" role="tabpanel"
                                 aria-labelledby="nav-password-tab">

                                <form method="POST" action="{{ route('user.updatePassword', $user) }}">
                                    @method('PUT')
                                    @csrf

                                    <div class="form-group row">
                                        <label for="password"
                                               class="col-md-4 col-form-label text-md-right">{{ __('Wachtwoord*') }}</label>

                                        <div class="col-md-6">
                                            <input id="password" type="password"
                                                   class="form-control @error('password') is-invalid @enderror"
                                                   name="password"
                                                   required autocomplete="new-password">

                                            @error('password')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="password-confirm"
                                               class="col-md-4 col-form-label text-md-right">{{ __('Wachtwoord bevestigen*') }}</label>

                                        <div class="col-md-6">
                                            <input id="password-confirm" type="password" class="form-control"
                                                   name="password_confirmation" required autocomplete="new-password">
                                        </div>
                                    </div>

                                    <div class="form-group row mb-0">
                                        <div class="col-md-6 offset-md-4">
                                            <button type="submit" class="btn btn-primary">
                                                {{ __('Wachtwoord bewerken') }}
                                            </button>

                                            <a class="text-secondary"
                                               href="{{ route('admin.users') }}">Terug
                                            </a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
