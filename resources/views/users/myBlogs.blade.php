@extends('layouts.app')

@section('content')
    <div class="container">
        @if (session('danger'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                {{ session('danger') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif

        @if (session('success'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                {{ session('success') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif

        <div class="row">
            <div class="col-md-3">
                <div class="card">
                    <div class="card-header text-center">
                        Profiel foto
                    </div>

                    <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist"
                         aria-orientation="vertical" style="padding-left: 10px;">

                        <div class="col-md-12 col-form-label text-md-center">
                            @if($user->profilepicture == null)
                                <img height="200px" width="200px" src="/img/profile/default/default.png" alt="">
                            @else
                                <img height="200px" width="200px" src="/img/profile/{{ $user->profilepicture }}" alt="">
                            @endif
                        </div>
                    </div>

                    <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                        <a class="nav-link text-center" id="v-pills-team-tab" href="{{ route('user.profile', $user) }}"
                           role="tab"
                           aria-controls="v-pills-team" aria-selected="false">Mijn profiel</a>
                        <a class="nav-link text-center" id="v-pills-team-tab" href="{{ route('user.myBlogs', $user) }}"
                           role="tab" aria-controls="v-pills-team" aria-selected="true"><b>Mijn blogs</b></a>
                    </div>
                </div>
            </div>

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">
                        <h1 style="display: inline-block;">Mijn Blogs</h1>

                        <div style="display: inline-block; float: right;">
                            <a class="btn btn-secondary"
                               href="{{ route('blog.create') }}">{{ ('Nieuwe Blog maken') }}</a>
                        </div>
                    </div>
                    <table class="rwd-table">
                        <tbody>
                        <tr>
                            <th style="padding-left: 10px; padding-top: 10px">Titel</th>
                            <th style="padding-top: 10px">Created at</th>
                            <th style="padding-top: 10px">Updated at</th>
                            <th></th>
                            <th></th>
                        </tr>
                        @foreach($blogs as $blog)
                            <tr>
                                <td data-th="Title" style="padding-left: 10px">
                                    <a href="{{ route('blog.show', $blog) }}">
                                        {{ $blog->title }}
                                    </a>
                                </td>
                                <td data-th="Created_at">
                                    @if($blog->created_at)
                                        {{ date_format($blog->created_at, 'd-m-Y') }}
                                    @endif
                                </td>
                                <td data-th="Updated_at">
                                    @if($blog->updated_at)
                                        {{ date_format($blog->updated_at, 'd-m-Y') }}
                                    @endif
                                </td>
                                <td data-th="Acties">
                                    <form method="POST" action="{{ route('blog.destroy', $blog) }}">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit"
                                                onclick="return confirm('weet je zeker dat je de blog {{$blog->title}} wilt verwijderen?')"
                                                class="btn text-secondary">Verwijderen
                                        </button>
                                    </form>
                                </td>
                                <td data-th="Acties">
                                    <a href="{{ route('blog.edit', $blog) }}"
                                       class="btn btn-primary">Bewerken
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
