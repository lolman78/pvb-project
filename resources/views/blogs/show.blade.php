@extends('layouts.app')

@section('content')
    <div class="container">
        @if (session('danger'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                {{ session('danger') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif

        @if (session('success'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                {{ session('success') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif

        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card" style="margin-bottom: 20px">
                    <div class="card-header"></div>
                    <div class="card-body">
                        <h1>{{ $blog->title }}</h1>

                        {!! $blog->content !!}

                        @if($blog->image !== null)
                            <img height="100%" width="100%"
                                 src="/img/blogs/{{ $blog->image }}" alt="">
                        @endif
                    </div>

                    <div class="row">
                        <p style="margin-left: 20px;">
                        @can('editAndUpdate', $blog)
                            <div class="col-md-2 ">
                                <a href="{{ route('blog.edit', $blog) }}"
                                   class="btn btn-primary">Bewerken
                                </a>
                            </div>
                        @endcan

                        @can('destroy', $blog)
                            <div class="col-md-2 ">
                                <form method="POST"
                                      action="{{ route('blog.destroy', $blog) }}">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit"
                                            onclick="return confirm('weet je zeker dat je deze blog wilt verwijderen?')"
                                            class="btn text-secondary">Verwijderen
                                    </button>
                                </form>
                            </div>
                        @endcan

                        <div class="col-md-2" style="margin-top: 6px">
                            <a class="text-secondary"
                               href="{{ route('home') }}">Terug
                            </a>
                        </div>
                        </p>
                    </div>

                    <div class="card-footer" style="margin-top: 20px;">
                        @if(!$currentCategory)
                            {{ "n.v.t." }}
                        @else
                            Category: <i>{{ $currentCategory->name }}</i>
                        @endif

                    </div>
                </div>
            </div>
        </div>
        @can('loggedIn', Auth()->user())
            <div class="row">
                <div class="card col-md-8 offset-sm-2" style="margin-bottom: 20px">
                    <div class="card-body">
                        <form method="POST" action="{{ route('comment.store', $blog) }}">
                            @csrf

                            <div class="form-group row">
                                <label for="content"
                                       class="col-md-1 col-form-label text-md-right"></label>
                            </div>

                            <div class="row">

                                <div class="col-md-2">
                                    @if(Auth::user()->profilepicture == null)
                                        <img height="100px" width="100px"
                                             src="/img/profile/default/default.png" alt="">
                                    @else
                                        <img height="100px" width="100px"
                                             src="/img/profile/{{ Auth::user()->profilepicture }}" alt="">
                                    @endif
                                </div>

                                <div class="col-md-10">
                                    <textarea id="content" class="form-control
                                             @error('content') is-invalid @enderror"
                                              name="content" autocomplete="content" rows="3"
                                              placeholder="Voeg een openbare reactie toe...">{{ old('content') }}</textarea>

                                    @error('content')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-4 offset-md-9">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Comment aanmaken') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-8 offset-sm-2">
                    @foreach($comments as $comment)
                        <div class="card" style="margin-bottom: 20px">
                            <div class="card-body ">
                                <div class="row">
                                    <div class="col-md-2">
                                        @if($comment->user->profilepicture == null)
                                            <img height="100px" width="100px"
                                                 src="/img/profile/default/default.png" alt="">
                                        @else
                                            <img height="100px" width="100px"
                                                 src="/img/profile/{{ $comment->user->profilepicture }}" alt="">
                                        @endif
                                    </div>
                                    <div class="col-md-9">
                                        {!! $comment->content !!}
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-7" style="margin-top: 5px; margin-left: 25px;">
                                        {{ $comment->user->name }}
                                    </div>

                                    @can('editAndUpdate', $comment)
                                        <a href="{{ route('comment.edit', $comment) }}"
                                           class="col-md-2 btn btn-primary">Bewerken
                                        </a>

                                        <div class="col-md-2">
                                            <form method="POST"
                                                  action="{{ route('comment.destroy', [$blog, $comment]) }}">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit"
                                                        onclick="return confirm('weet je zeker dat je deze activiteit wilt verwijderen?')"
                                                        class="btn text-secondary">Verwijderen
                                                </button>
                                            </form>
                                        </div>
                                    @endcan
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        @endcan
    </div>
@endsection
