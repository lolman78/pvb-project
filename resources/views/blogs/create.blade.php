@extends('layouts.app')

@section('content')
    <div class="container">
        @if (session('danger'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                {{ session('danger') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif

        @if (session('success'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                {{ session('success') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif

        <div class="row justify-content-center">
            @can('isAdmin', Auth::User())
                <div class="col-md-3 p-0">
                    <div class="card">
                        <div class="card-header">
                            Admin menu
                        </div>
                        <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist"
                             aria-orientation="vertical">
                            <a class="nav-link" id="v-pills-team-tab" href="{{ route('admin.users') }}" role="tab"
                               aria-controls="v-pills-team" aria-selected="false">Gebruikers</a>
                            <a class="nav-link" id="v-pills-team-tab" href="{{ route('admin.blogs') }}"
                               role="tab" aria-controls="v-pills-team" aria-selected="true"><b>Blogs</b></a>
                            <a class="nav-link" id="v-pills-club-tab" href="{{ route('admin.comments') }}" role="tab"
                               aria-controls="v-pills-club" aria-selected="false">Comments</a>
                            <a class="nav-link" id="v-pills-user-tab" href="{{ route('admin.categories') }}" role="tab"
                               aria-controls="v-pills-user" aria-selected="false">Categorieen</a>
                            <a class="nav-link" id="v-pills-team-tab" href="{{ route('admin.roles') }}"
                               role="tab" aria-controls="v-pills-team" aria-selected="false">Rollen</a>
                        </div>
                    </div>
                </div>
            @endcan

            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Nieuwe blog maken') }}</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('blog.store') }}" enctype="multipart/form-data">
                            @csrf

                            <div class="form-group row">
                                <label for="title"
                                       class="col-md-4 col-form-label text-md-right">{{ __('Titel') }}</label>

                                <div class="col-md-6">
                                    <input id="title" type="text"
                                           class="form-control @error('title') is-invalid @enderror" name="title"
                                           value="{{ old('title') }}" autocomplete="title" autofocus>

                                    @error('title')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="content"
                                       class="col-md-4 col-form-label text-md-right">{{ __('Inhoud') }}</label>

                                <div class="col-md-6">
                                    <textarea id="content" class="form-control
                                        @error('content') is-invalid @enderror"
                                              name="content" autocomplete="content" rows="6">
                                        {{ old('content') }}
                                    </textarea>

                                    @error('content')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="category"
                                       class="col-md-4 col-form-label text-md-right">{{ __('Categorie') }}</label>

                                <div class="col-md-6">
                                    <select id="category" class="form-control @error('category') is-invalid @enderror"
                                            name="category" value="{{ old('category') }}" required autocomplete="category"
                                            autofocus>
                                        @foreach($categories as $category)
                                            <option value="{{$category->id}}">{{$category->name}}</option>
                                        @endforeach
                                    </select>

                                    @error('category')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="image"
                                       class="col-md-4 col-form-label text-md-right">{{ __('Foto toevoegen') }}</label>

                                <div class="col-md-6">
                                    <input type="file" id="image" name="image"
                                           class="form-control @error('image') is-invalid @enderror">

                                    @error('image')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Blog maken') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="node_modules/@ckeditor/ckeditor5-build-classic/build/ckeditor.js"></script>
    <script>
        ClassicEditor
            .create(document.querySelector('#content'), {
                removePlugins: ['bulletedList', 'numberedList'],
                toolbar: ['Heading', 'bold', 'italic', 'Link', 'blockQuote']
            })
            .then(content => {
                console.log(content);

                const data = content.getData();
                console.log(data);
            })
            .catch(error => {
                console.error(error);
            });
    </script>
@endsection
