@extends('layouts.app')

@section('content')
    <div class="container">
        @if (session('danger'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                {{ session('danger') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif

        @if (session('success'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                {{ session('success') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif

        <div class="container">
            <div class="row">
                @foreach($blogs as $blog)
                    <div class="col-md-4" style="margin-top: 20px;">
                        <div class="card">                       
                            <a
                                href="{{ route('blog.show', $blog->id) }}">
                                @if($blog->image == null)
                                    <img height="200px" width="100%"
                                         src="img/blogs/default/default.png" alt="">
                                @else
                                    <img height="200px" width="100%"
                                         src="/img/blogs/{{ $blog->image }}"
                                         alt="img/blogs/default/default.png">
                                @endif
                            </a>

                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h3>{{ $blog->title }}</h3>
                                        <p>
                                            {!! substr($blog->content, 0, 100)!!}
                                        </p>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-7" style="margin-top: 5px;">
                                        <p>
                                            <span class="text-secondary">Auteur: </span>{{ $blog->user->name }}
                                            <br>
                                            <span class="text-secondary">Datum: </span>{{ date_format( $blog->created_at, 'd-m-Y') }}
                                        </p>
                                    </div>
                                    <div class="col-md-5" style="margin-top: 15px;">
                                        <a href="{{ route('blog.show', $blog) }}"
                                           class="btn btn-primary">Lees meer
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
@endsection
