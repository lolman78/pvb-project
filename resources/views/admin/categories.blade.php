@extends('layouts.app')

@section('content')
    <div class="container">
        @if (session('danger'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                {{ session('danger') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif

        @if (session('success'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                {{ session('success') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif

        <div class="row mb-4">
            <div class="col-md-3 p-0">
                <div class="card">
                    <div class="card-header">
                        Admin menu
                    </div>
                    <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                        <a class="nav-link" id="v-pills-team-tab" href="{{ route('admin.users') }}" role="tab"
                           aria-controls="v-pills-team" aria-selected="false">Gebruikers</a>
                        <a class="nav-link" id="v-pills-team-tab" href="{{ route('admin.blogs') }}"
                           role="tab" aria-controls="v-pills-team" aria-selected="false">Blogs</a>
                        <a class="nav-link" id="v-pills-club-tab" href="{{ route('admin.comments') }}" role="tab"
                           aria-controls="v-pills-club" aria-selected="false">Comments</a>
                        <a class="nav-link" id="v-pills-user-tab" href="{{ route('admin.categories') }}" role="tab"
                           aria-controls="v-pills-user" aria-selected="true"><b>Categorieen</b></a>
                        <a class="nav-link" id="v-pills-team-tab" href="{{ route('admin.roles') }}"
                           role="tab" aria-controls="v-pills-team" aria-selected="false">Rollen</a>
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">
                        <h1 style="display: inline-block;">Categorieen</h1>

                        <div class="dropdown" style="display: inline-block; float: right;">
                            <button class="btn btn-secondary  float-right" type="button" id="dropdownMenuButton"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Acties
                            </button>

                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item btn float-right" href="{{ route('user.create') }}">
                                    Nieuwe gebruiker maken
                                </a>
                                <a class="dropdown-item btn float-right"
                                   href="{{ route('blog.create') }}">{{ ('Nieuwe Blog maken') }}
                                </a>
                                <a class="dropdown-item btn float-right"
                                   href="{{ route('category.create') }}">{{ ('Nieuwe Categorie maken') }}
                                </a>
                                <a class="dropdown-item btn float-right"
                                   href="{{ route('role.create') }}">{{ ('Nieuwe rol Maken') }}
                                </a>
                            </div>
                        </div>
                    </div>
                    <table class="rwd-table">
                        <tbody>
                        <tr>
                            <th style="padding-left: 10px; padding-top: 10px">Naam</th>
                            <th>Created at</th>
                            <th style="padding-left: 14px;">Acties</th>
                            <th></th>
                            <th></th>
                        </tr>
                        @foreach($categories as $category)
                            <tr>
                                <td data-th="Titel" style="padding-left: 10px">
                                    {{ $category->name }}
                                </td>
                                <td data-th="Created_at">
                                    {{ date_format($category->created_at, 'd-m-Y') }}
                                </td>
                                <td data-th="Acties">
                                    <form method="POST" action="{{ route('category.destroy', $category) }}">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit"
                                                onclick="return confirm('weet je zeker dat je de {{$category->name}} wilt verwijderen?')"
                                                class="btn text-secondary">Verwijderen
                                        </button>
                                    </form>
                                </td>
                                <td data-th="Acties">
                                    <a href="{{ route('category.edit', $category) }}"
                                       class="btn btn-primary">Bewerken</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
