{{-- Welkom de user op de website --}}
<h1>Welkom {{ $name }}</h1>

{{-- Dit is het email adres dat ze ingevuld hebben --}}
<p>U heeft zo net een Gebuikers account gemaakt, met het Email adres: {{$email}}</p>

{{-- Geef ze een inlog link --}}
<p>Als u op deze knop klikt kunt u inloggen op de site met uw nieuwe gebruikers account:</p>

<p>
    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
</p>
