@extends('layouts.app')

@section('content')
    <div class="container">
        @if (session('danger'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                {{ session('danger') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif

        @if (session('success'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                {{ session('success') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif

        <div class="row justify-content-center">
            <div class="col-md-3 p-0">
                <div class="card">
                    <div class="card-header">
                        Admin menu
                    </div>
                    <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                        <a class="nav-link" id="v-pills-team-tab" href="{{ route('admin.users') }}" role="tab"
                           aria-controls="v-pills-team" aria-selected="false">Gebruikers</a>
                        <a class="nav-link" id="v-pills-team-tab" href="{{ route('admin.blogs') }}"
                           role="tab" aria-controls="v-pills-team" aria-selected="false">Blogs</a>
                        <a class="nav-link" id="v-pills-club-tab" href="{{ route('admin.comments') }}" role="tab"
                           aria-controls="v-pills-club" aria-selected="false">Comments</a>
                        <a class="nav-link" id="v-pills-user-tab" href="{{ route('admin.categories') }}" role="tab"
                           aria-controls="v-pills-user" aria-selected="true"><b>Categorieen</b></a>
                        <a class="nav-link" id="v-pills-team-tab" href="{{ route('admin.roles') }}"
                           role="tab" aria-controls="v-pills-team" aria-selected="false">Rollen</a>
                    </div>
                </div>
            </div>

            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Categorie bewerken') }}</div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('category.update', $category )}}"
                              enctype="multipart/form-data">
                            @method('PUT')
                            @csrf

                            <div class="form-group row">
                                <label for="name"
                                       class="col-md-4 col-form-label text-md-right">{{ __('Naam') }}
                                </label>

                                <div class="col-md-6">
                                    <input id="name" type="text"
                                           class="form-control @error('name') is-invalid @enderror" name="name"
                                           value="{{ old('name', $category->name) }}" autocomplete="name" autofocus>

                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Categorie aanpassen') }}
                                    </button>

                                    <a class="text-secondary"
                                       href="{{ route('admin.categories') }}">Terug
                                    </a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
