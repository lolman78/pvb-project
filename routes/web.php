<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

// Admin
Route::get('/admin/users', 'AdminController@users')->name('admin.users');
Route::get('/admin/blogs', 'AdminController@blogs')->name('admin.blogs');
Route::get('/admin/comments', 'AdminController@comments')->name('admin.comments');
Route::get('/admin/categories', 'AdminController@categories')->name('admin.categories');
Route::get('/admin/roles', 'AdminController@roles')->name('admin.roles');

// Blogs
Route::get('/blog/index', 'BlogController@index')->name('blog.index');
Route::get('/blog/create', 'BlogController@create')->name('blog.create');
Route::post('/blog/store', 'BlogController@store')->name('blog.store');
Route::get('/blog/{blog}/show', 'BlogController@show')->name('blog.show');
Route::get('/blog/{blog}/edit', 'BlogController@edit')->name('blog.edit');
Route::put('/blog/{blog}/update', 'BlogController@update')->name('blog.update');
Route::delete('/blog/{blog}/delete', 'BlogController@destroy')->name('blog.destroy');
Route::get('/blog/{blog}/deleteImage', 'BlogController@destroyImage')->name('blog.destroyImage');

// Users
Route::get('/user/index', 'UserController@index')->name('user.index');
Route::get('/user/create', 'UserController@create')->name('user.create');
Route::post('/user/store', 'UserController@store')->name('user.store');
Route::get('/user/{user}/show', 'UserController@show')->name('user.show');
Route::get('/user/{user}/edit', 'UserController@edit')->name('user.edit');
Route::put('/user/{user}/update', 'UserController@update')->name('user.update');
Route::delete('/user/{user}/delete', 'UserController@destroy')->name('user.destroy');
Route::get('/user/{user}/profile', 'UserController@profile')->name('user.profile');
Route::get('/user/{user}/myBlogs', 'UserController@myBlogs')->name('user.myBlogs');
Route::put('/user/{user}/updatePassword', 'UserController@updatePassword')->name('user.updatePassword');
Route::put('/user/{user}/updateProfilePicture', 'UserController@updateProfilePicture')->name('user.updateProfilePicture');
Route::get('/user/confirmUser', 'MailController@confirmationUser')->name('user.confirm');

// Comments
Route::get('/comment/index', 'CommentController@index')->name('comment.index');
Route::get('/comment/create', 'CommentController@create')->name('comment.create');
Route::post('/blog/{blog}/comment/store', 'CommentController@store')->name('comment.store');
Route::get('/blog/{blog}/comment/{comment}/show', 'CommentController@show')->name('comment.show');
Route::get('/comment/{comment}/edit', 'CommentController@edit')->name('comment.edit');
Route::put('/blog/{blog}/comment/{comment}/update', 'CommentController@update')->name('comment.update');
Route::delete('/blog/{blog}/comment/{comment}/delete', 'CommentController@destroy')->name('comment.destroy');

// Categories
Route::get('/category/index', 'CategoryController@index')->name('category.index');
Route::get('/category/create', 'CategoryController@create')->name('category.create');
Route::post('/category/store', 'CategoryController@store')->name('category.store');
Route::get('/category/{category}/show', 'CategoryController@show')->name('category.show');
Route::get('/category/{category}/edit', 'CategoryController@edit')->name('category.edit');
Route::put('/category/{category}/update', 'CategoryController@update')->name('category.update');
Route::delete('category/{category}/delete', 'CategoryController@destroy')->name('category.destroy');

// Roles
Route::get('/role/index', 'RoleController@index')->name('role.index');
Route::get('/role/create', 'RoleController@create')->name('role.create');
Route::post('/role/store', 'RoleController@store')->name('role.store');
Route::get('/role/{role}/show', 'RoleController@show')->name('role.show');
Route::get('/role/{role}/edit', 'RoleController@edit')->name('role.edit');
Route::put('/role/{role}/update', 'RoleController@update')->name('role.update');
Route::delete('role/{role}/delete', 'RoleController@destroy')->name('role.destroy');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
