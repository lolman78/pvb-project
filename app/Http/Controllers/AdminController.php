<?php

namespace App\Http\Controllers;

use App\Blog;
use App\Category;
use App\Comment;
use App\Role;
use App\User;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function users()
    {
        $this->authorize('isAdmin', User::class);
        $users = User::orderBy('name')->get();

        return view('admin.users', ['users' => $users]);
    }

    public function blogs()
    {
        $this->authorize('isAdmin', User::class);
        $blogs = Blog::orderBy('created_at')->get();

        return view('admin.blogs', ['blogs' => $blogs]);
    }

    public function comments()
    {
        $this->authorize('isAdmin', User::class);
        $comments = Comment::orderBy('created_at')->get();

        return view('admin.comments', ['comments' => $comments]);
    }

    public function categories()
    {
        $this->authorize('isAdmin', User::class);
        $categories = Category::orderBy('name')->get();

        return view('admin.categories', ['categories' => $categories]);
    }

    public function roles()
    {
        $this->authorize('isAdmin', User::class);
        $roles = Role::orderBy('name')->get();

        return view('admin.roles', ['roles' => $roles]);
    }
}
