<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Blog;
use App\Http\Requests\StoreCommentRequest;
use App\Http\Requests\UpdateCommentRequest;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCommentRequest $request, $blog_id)
    {
        $blog = Blog::findOrFail($blog_id);
        $this->authorize('createAndStore', Comment::class);

        return redirect()->route('blog.show', $blog)->with('success', 'Comment aangemaakt');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function show(Comment $comment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function edit(Comment $comment)
    {
        $blog = Blog::findOrFail($comment->blog_id);
        $this->authorize('editAndUpdate', $comment);

        return view('comments.edit', ['comment' => $comment, 'blog' => $blog]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCommentRequest $request, Blog $blog, Comment $comment)
    {
        $comment->update(request()->all());
        $this->authorize('editAndUpdate', $comment);

        return redirect()->route('blog.show', $blog)->with('success', 'Comment bewerkt');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Blog $blog, Comment $comment)
    {
        $this->authorize('destroy', $comment);
        $comment->delete();

        return back()->with('success', 'Comment verwijderd', compact('blog'));
    }
}
