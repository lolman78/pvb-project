<?php

namespace App\Http\Controllers\Auth;

use Auth;
use Mail;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'address' => ['max:255'],
            'postalcode' => ['max:255'],
            'place' => ['max:255'],
            'phonenumber' => ['max:255'],
            'role_id' => ['required | min:1'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */

    protected function create(array $data)
    {
        request()->session()->flash('success', 'Welkom nieuwe gebruiker!');

        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'address' => $data['address'],
            'postalcode' => $data['postalcode'],
            'place' => $data['place'],
            'phonenumber' => $data['phonenumber'],
            'role_id' => 1,
            'password' => Hash::make($data['password']),
        ]);

        // email data
        $email_data = array(
            'name' => $data['name'],
            'email' => $data['email'],
        );

        // send email with the template
        Mail::send('mail.confirmUserMail', $email_data, function ($message) use ($email_data, $user) {
            $message->to($email_data['email'], $email_data['name'])
                ->subject('Welkom Gebuiker')
                ->from('admin@gmail.com', 'MyNotePaper');
        });

        return $user;
    }
}
