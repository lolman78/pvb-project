<?php

namespace App\Http\Controllers;

use App\Blog;
use App\Category;
use App\Comment;
use App\Http\Requests\StoreBlogRequest;
use App\Http\Requests\UpdateBlogRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('createAndStore', Blog::class);
        $categories = Category::orderBy('name')->get();

        return view('blogs.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBlogRequest $request)
    {
        $this->authorize('createAndStore', Blog::class);

//      Haal de nieuwe aangemaakte blog op uit de database.
//      Haal de juiste category op uit de database en koppel het aan de nieuwe blog.
        $blog = Blog::latest()->first();
        $category = Category::findOrFail($request->category);

        $blog->categories()->attach($category->id);

//      Kijk of er een foto in de request zit die gekoppeld moet worden aan de blog.
//      Zo ja sla het plaatje op en koppel het plaatje aan de blog.
        if ($request->hasFile('image')) {
            $extension = $request->image->extension();
            $imageName = Str::snake($blog->title) . '.' . $extension;

            request()->image->move(public_path('img/blogs/'), $imageName);

            $blog->image = $imageName;
        }

        $blog->save();

        return redirect('/')->with('success', 'Nieuwe blog aangemaakt');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Blog $blog
     * @return \Illuminate\Http\Response
     */
    public function show(Blog $blog)
    {
        // Iedereen mag de blogs bekijken dus hier is geen policy nodig
        $comments = Comment::orderBy('created_at', 'desc')->where('blog_id', '=', $blog->id)->get();
        $currentCategory = $blog->categories()->first();

        return view('blogs.show', compact('blog', 'blog', 'comments', 'currentCategory'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Blog $blog
     * @return \Illuminate\Http\Response
     */
    public function edit(Blog $blog)
    {
        $this->authorize('editAndUpdate', $blog);
        $categories = Category::orderBy('name')->get();
        $currentCategory = $blog->categories()->first();

        return view('blogs.edit', compact('categories', 'blog', 'currentCategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Blog $blog
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateBlogRequest $request, Blog $blog)
    {
        $this->authorize('editAndUpdate', $blog);

//      De blog word bijgewerkt
//      Daarna kijk je of er een foto in de request zit die gekoppeld moet worden aan de blog.
//      Zo ja dan word het plaatje gekoppeld aan de blog.
//      En haal de oude foto die gekoppled is aan de blog eruit (tenzij het de default afbeelding is)
        $blog->update(request()->all());

        $category = Category::findOrFail($request->category);
        $blog->categories()->sync($category->id);

        if ($request->hasFile('image')) {
//          haal het oude plaatje eruit voordat je de nieuwe erin zet.
            if (file_exists(public_path('img/blogs/' . $blog->image))) {

//              als de image die eraan gekoppeld de default is hoef je de image niet te verwijderen
                if ($blog->image !== null) {
                    unlink(public_path('img/blogs/' . $blog->image));
                }

                $extension = $request->image->extension();
                $imageName = Str::snake($blog->title) . '.' . $extension;

                request()->image->move(public_path('img/blogs/'), $imageName);
                $blog->image = $imageName;

                $blog->save();

            } else {

                dd('File does not exists.');

            }
        }

        return redirect()->route('blog.show', ['blog' => $blog])->with('success', 'Blog bewerkt');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Blog $blog
     * @return \Illuminate\Http\Response
     */
    public function destroy(Blog $blog)
    {
        $this->authorize('destroy', $blog);
        $comments = Comment::orderBy('created_at')->where('blog_id', '=', $blog->id);

        $comments->delete();
        $blog->delete();

        if (request()->getRequestUri() === '/blog/' . $blog->id . '/delete') {
            return redirect('/')->with('success', 'Blog verwijderd');
        }

        return back()->with('success', 'Blog verwijderd');
    }

    public function destroyImage(Blog $blog)
    {
        unlink(public_path('img/blogs/' . $blog->image));
        $blog->image = null;
        $blog->save();

        return redirect()->route('blog.show', ['blog' => $blog])->with('success', 'Blog plaatje bewerkt');
    }
}
