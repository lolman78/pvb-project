<?php

namespace App\Http\Controllers;

use Auth;
use Mail;
use Illuminate\Http\Request;

class MailController extends Controller
{
    public function confirmationUser()
    {
        $user = Auth::user();

        Mail::send('mail.confirmUserMail', ['user' => $user], function ($message) use ($user)
        {
            $message->from("admin@gmail.com");
            $message->to('georgeanti99@gmail.com', '');
            $message->subject("Gebruiker registreren");
        });

        return back();
    }
}
