<?php

namespace App\Http\Controllers;

use App\Blog;
use App\Comment;
use App\User;
use App\Role;
use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserPasswordRequest;
use App\Http\Requests\UpdateUserRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('createAndStore', User::class);
        $roles = Role::orderBy('created_at')->get();

        return view('users.create', ['roles' => $roles]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUserRequest $request)
    {
        $this->authorize('createAndStore', User::class);
        return redirect()->route('admin.users')->with('success', 'Gebruiker aangemaakt');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $this->authorize('editAndUpdate', $user);
        $roles = Role::orderBy('created_at')->get();

        return view('users.edit', ['roles' => $roles, 'user' => $user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserRequest $request, User $user)
    {
        $this->authorize('editAndUpdate', $user);
        $user->update(request()->all());

//      Kijk of er een foto in de request zit die gekoppeld moet worden aan de user.
//      Zo ja sla het plaatje op en koppel het plaatje aan de user.
        if($request->hasFile('profile_picture'))
        {
//          haal het oude plaatje eruit voordat je de nieuwe erin zet.
            if(file_exists(public_path('img/profile/'.$user->profilepicture))){

//              als de image die eraan gekoppeld de default is hoef je de image niet te verwijderen
                if($user->profilepicture !== null)
                {
                    unlink(public_path('img/profile/'.$user->profilepicture));
                }

                $extension = $request->profile_picture->extension();
                $imageName = Str::snake($user->name) . '.' . $extension;

                request()->profile_picture->move(public_path('img/profile/'), $imageName);
                $user->profilepicture = $imageName;

                $user->save();

            }else{

                dd('File does not exists.');

            }
        }

        return back()->with('success', 'Gebruiker bewerkt');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $this->authorize('destroy', $user);

//      Verwijder alle blogs en de comments die horen bij die blogs die horen bij de verwijderde user
//      Verwijder alle comments die bij de verwijderde user horen
        $blogs = Blog::orderBy('created_at')->where('user_id', '=', $user->id)->get();

//      Als er geen blogs zijn hoef je de blogs verwijderen stuk van de code niet uit te voeren
        if($blogs)
        {
            foreach($blogs as $blog)
            {
//              Verwijder all comments die horen bij de blogs die je gaat verwijderen
                $comments = Comment::orderBy('created_at')->where('blog_id', '=', $blog->id)->get();

//              Verwijder de profiel foto van de blog uit je img folder
//              als de image die eraan gekoppeld de default is hoef je de image niet te verwijderen
                if($blog->image !== "default.png")
                {
                    unlink(public_path('img/blogs/'.$blog->image));
                }

                if($comments)
                {
                    foreach($comments as $comment)
                    {
                        $comment->delete();
                    }
                }
            }

            foreach($blogs as $blog)
            {
                $blog->delete();
            }
        }

        $comments = Comment::orderBy('created_at')->where('user_id', '=', $user->id)->get();
        if($comments)
        {
            foreach($comments as $comment)
            {
                $comment->delete();
            }
        }

//      als de image die eraan gekoppeld de default is hoef je de image niet te verwijderen
        if($user->profilepicture !== "default.png")
        {
            unlink(public_path('img/profile/'.$user->profilepicture));
        }

        $user->delete();

        return redirect()->route('admin.users')->with('success', 'Gebruiker verwijderd');
    }

    public function profile(User $user)
    {
        $this->authorize('loggedIn', $user);
        $roles = Role::orderBy('created_at')->get();

        return view('users.profile', ['user' => $user, 'roles' => $roles]);
    }

    public function myBlogs(User $user)
    {
        $this->authorize('loggedIn', $user);
        $blogs = Blog::orderBy('created_at')->where('user_id', '=', $user->id)->get();

        return view('users.myBlogs', ['user' => $user, 'blogs' => $blogs]);
    }

    public function updatePassword(UpdateUserPasswordRequest $request, User $user)
    {
        $this->authorize('updatePassword', $user);

        request()->merge(
            [
                'password' => bcrypt(request()->password),
            ]);

        $user->update(request()->all());

        return back()->with('success', 'Je wachtwoord is bewerkt!');
    }
}
