<?php

namespace App\Http\Requests;

use Auth;
use App\Blog;
use Illuminate\Foundation\Http\FormRequest;

class StoreBlogRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */

    public function authorize()
    {
        return true;
    }

    protected function prepareForValidation()
    {
        $user_id = Auth::User()->id;

        // zet de content in een variabele
        $content = Request()->get("content");

        // haal alle <p> tags eruit
        $content = str_replace("<p>", "", $content);

        // verander de </p> tags met <br>
        $content = str_replace("</p>", "<br>", $content);

        // merge de nieuwe versie van de content in de request
        request()->merge(
            [
                'user_id' => $user_id,
                'content' => $content
            ]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            Blog::create(request()->validate([
                'title' => 'required | max:255 ',
                'content' => 'required | max:5000 ',
                'user_id' => 'required'
            ]))
        ];
    }
}
