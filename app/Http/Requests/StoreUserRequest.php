<?php

namespace App\Http\Requests;

use Axlon\PostalCodeValidation\Rules\PostalCode;
use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Hash;

class StoreUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function prepareForValidation()
    {
        request()->merge(
            [
                'password' => bcrypt(request()->password),
            ]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            User::create(request()->validate([
                'name' => 'required | max:255',
                'email' => 'required | string | email | max:255',
                'address' => 'nullable | max:255',
                'postalcode' =>  'nullable | '.PostalCode::forCountry('NL'),
                'place' => 'nullable | max:255',
                'phonenumber' => 'nullable | regex:/^([0-9\s\-\+\(\)]*)$/|min:9',
                'role_id' => 'required | min:1',
                'password' => 'required | string | min:8',
            ]))
        ];
    }
}
