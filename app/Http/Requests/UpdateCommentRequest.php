<?php

namespace App\Http\Requests;

use Auth;
use Illuminate\Foundation\Http\FormRequest;

class UpdateCommentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function prepareForValidation()
    {
        $user_id = Auth::User()->id;

        request()->merge(
            [
                'user_id' => $user_id
            ]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return
        [
            [
                request()->validate(
                [
                    'content' => 'required | max:255 ',
                    'user_id' => 'required',
                ])
            ]
        ];
    }
}
