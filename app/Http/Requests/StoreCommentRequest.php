<?php

namespace App\Http\Requests;

use Auth;
use App\Comment;
use App\Blog;
use Illuminate\Foundation\Http\FormRequest;

class StoreCommentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */

    public function authorize()
    {
        return true;
    }

    protected function prepareForValidation()
    {
        $user_id = Auth::User()->id;
        $blog_id = request()->route('blog');

        request()->merge(
            [
                'user_id' => $user_id,
                'blog_id' => $blog_id
            ]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            Comment::create(request()->validate([
                'content' => 'required | max:255 ',
                'user_id' => 'required',
                'blog_id' => 'required'
            ]))
        ];
    }
}
