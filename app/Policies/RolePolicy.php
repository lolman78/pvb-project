<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class RolePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function createAndStore(User $user)
    {
//      Als de role van de user: admin is mogen ze een role maken.
        return $user->role->name == 'admin';
    }

    public function editAndUpdate(User $user)
    {
//      Als de role van de user: admin is mogen ze de role bewerken.
        return $user->role->name == 'admin';
    }

    public function destroy(User $user)
    {
//      Als de role van de user: admin is mogen ze de role verwijderen.
        return $user->role->name == 'admin';
    }
}
