<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class CategoryPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function createAndStore(User $user)
    {
//      Als de role van de user: admin is mogen ze een category maken.
        return $user->role->name == 'admin';
    }

    public function editAndUpdate(User $user)
    {
//      Als de role van de user: admin is mogen ze de category bewerken.
        return $user->role->name == 'admin';
    }

    public function destroy(User $user)
    {
//      Als de role van de user: admin is mogen ze de category verwijderen.
        return $user->role->name == 'admin';
    }
}
