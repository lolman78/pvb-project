<?php

namespace App\Policies;

use App\User;
use App\Comment;
use Illuminate\Auth\Access\HandlesAuthorization;

class CommentPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function createAndStore(User $user)
    {
//      Als de role van de user: user of admin is mogen ze een comment maken.
        return ($user->role->name == 'user') || ($user->role->name == 'admin');
    }

    public function editAndUpdate(User $user, Comment $comment)
    {
//      Als de comment van de ingelogde user: user is of de user is een admin mogen ze de comment aanpassen.
        return ($comment->user_id == $user->id) || ($user->role->name == 'admin');
    }

    public function destroy(User $user, Comment $comment)
    {
//      Als de comment van de ingelogde user: user is of de user is een admin mogen ze de comment verwijderen.
        return ($comment->user_id == $user->id) || ($user->role->name == 'admin');
    }
}
