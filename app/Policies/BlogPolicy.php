<?php

namespace App\Policies;

use App\User;
use App\Blog;
use Illuminate\Auth\Access\HandlesAuthorization;

class BlogPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function createAndStore(User $user)
    {
//      Als de role van de user: user of admin is mogen ze een blog maken.
        return ($user->role->name == 'user') || ($user->role->name == 'admin');
    }

    public function editAndUpdate(User $user, Blog $blog)
    {
//      Als de blog van de ingelogde user: user is of de user is een admin mogen ze de blog aanpassen.
        return ($blog->user_id == $user->id) || ($user->role->name == 'admin');
    }

    public function destroy(User $user, Blog $blog)
    {
//      Als de blog van de ingelogde user: user is of de user is een admin mogen ze de blog verwijderen.
        return ($blog->user_id == $user->id) || ($user->role->name == 'admin');
    }
}
