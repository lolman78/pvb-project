<?php

namespace App\Policies;

use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function loggedIn(User $user)
    {
//      Als de role van de user: user of admin is mogen ze een comment maken.
        return ($user->role->name == 'user') || ($user->role->name == 'admin');
    }

    public function createAndStore(User $user)
    {
//      Als de role van de user: admin is mogen ze een user maken.
        return $user->role->name == 'admin';
    }

    public function editAndUpdate(User $user)
    {
//        dd($user->user_id == Auth::id());
//      Als de role van de user: admin is mogen ze de user bewerken.
        return ($user->id == Auth::id()) || ($user->role->name == 'admin');
    }

    public function destroy(User $user)
    {
//      Als de role van de user: admin is mogen ze een user verwijderen.
        return $user->role->name == 'admin';
    }

    public function isAdmin(User $user)
    {
        //  Kijk of de user de role admin of niet heeft
        return $user->role->name == 'admin';
    }

    public function updatePassword(User $user)
    {
//      Als de role van de user: user of admin is mogen ze een comment maken.
        return $user->role->name == Auth::id() || 'admin';
    }
}
